<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTest extends Model
{
    protected $table = 'user_tests';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'name', 'parity'
    ];
}
