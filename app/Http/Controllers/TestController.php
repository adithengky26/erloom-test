<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;
use App\Http\Requests\InputNameRequest;


class TestController extends Controller
{
    public function index()
    {
        $getAll = UserTest::get();
        return view('index', compact('getAll'));
    }

    public function store(InputNameRequest $request)
    {
        //LOGIC HERE
        try {
            $data = $request->all();
            $parity = 'Odd';
            $lastId = UserTest::latest('id')->first();
            if ($lastId) {
                if (($lastId->id + 1) % 2 == 0) {
                    $parity = 'Even';
                }
            } else {
                $data['id'] = 0;
                $parity = 'Even';
            }
            $data['parity'] = $parity;
            UserTest::create($data);
            return redirect()->route('index');
        } catch (\Exception $e) {
            return back()->withError($e->getMessage())->withInput();
        }
    }

}
